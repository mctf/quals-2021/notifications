package mctf.ru

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.LocalDateTime
import java.util.*

object Users : Table() {
    val id = uuid("id")
    val registerDate = datetime("register_date")
    override val primaryKey = PrimaryKey(id)
}

object Notifications : Table() {
    val id = uuid("id")
    val user = uuid("user_id").references(Users.id)
    val date = datetime("date")
    val text = varchar("text", 256)
    override val primaryKey = PrimaryKey(id)
}

data class Notification(
    val text: String,
    val date: LocalDateTime
)