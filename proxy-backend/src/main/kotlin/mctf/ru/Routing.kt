package mctf.ru

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import java.io.File
import java.util.*

fun Application.configureRouting(service: NotificationService) {
    install(Routing) {
        get("/getUserId") {
            val id = service.createNewUser()
            call.respond(id.toString())
        }

        get("/getNotifications") {
            val userId = call.request.header("X-User-Id")?.toUUID()
                ?: run {
                    call.respond(HttpStatusCode.BadRequest, "Id not found")
                    return@get
                }

            if (!service.userExists(userId)) {
                call.respond(HttpStatusCode.BadRequest, "User does not exist")
                return@get
            }

            val notifications = service.getNotifications(userId)
            call.respond(notifications)
        }

        post("/sendFlag") {
            val targetUser = call.receive<String>().toUUID()
                ?: run {
                    call.respond(HttpStatusCode.BadRequest, "Id not found")
                    return@post
                }

            if (!service.userExists(targetUser)) {
                call.respond(HttpStatusCode.BadRequest, "User does not exist")
                return@post
            }

            service.sendFlag(targetUser)
            call.response.status(HttpStatusCode.OK)
        }

        get("/healthcheck") {
            if (File("flag.txt").exists()) {
                call.respond(HttpStatusCode.OK, "Ok")
            } else {
                call.respond(HttpStatusCode.ServiceUnavailable, "Flag not found")
            }
        }

        static("/") {
            resources("static")
            defaultResource("static/index.html")
        }
    }
}

private fun String.toUUID() = runCatching { UUID.fromString(this) }.getOrNull()