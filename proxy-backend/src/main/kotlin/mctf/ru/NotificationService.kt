package mctf.ru

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import java.io.File
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*
import java.util.logging.Logger

class NotificationService {

    private val log: Logger = Logger.getLogger("GameService")
    private val welcomeMessage =
        "Welcome to our notifications system! Here you will find flags and other important stuff... maybe."

    suspend fun createNewUser(): UUID = dbQuery {
        val id = UUID.randomUUID()
        log.info("Creating user $id")

        Users.insert {
            it[Users.id] = id
            it[Users.registerDate] = LocalDateTime.now()
        }

        sendWelcomeMessage(id)
        id
    }

    suspend fun userExists(user: UUID) = dbQuery {
        Users.select { Users.id eq user }.empty().not()
    }

    suspend fun getNotifications(user: UUID) = dbQuery {
        Notifications.select { Notifications.user eq user }
            .orderBy(Notifications.date)
            .map {
                Notification(
                    text = it[Notifications.text],
                    date = it[Notifications.date],
                )
            }
    }

    suspend fun sendFlag(user: UUID) = dbQuery {
        val flag = File("flag.txt").readText()
        sendNotification(user, flag)
    }

    private fun sendWelcomeMessage(user: UUID) {
        sendNotification(user, welcomeMessage)
    }

    private fun sendNotification(user: UUID, text: String) {
        val id = UUID.randomUUID()

        Notifications.insert {
            it[Notifications.id] = id
            it[Notifications.user] = user
            it[Notifications.date] = LocalDateTime.now(ZoneOffset.UTC)
            it[Notifications.text] = text
        }
    }

}